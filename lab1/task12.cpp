#include "task12.h"


void ref_rotate(struct circle &s, double &delta_x, double &delta_y)
{
    s.x += delta_x;
    s.y += delta_y;
}

void pointer_rotate(struct circle *s, double *delta_x, double *delta_y)
{
    s->x += *delta_x;
    s->y += *delta_y;
}
