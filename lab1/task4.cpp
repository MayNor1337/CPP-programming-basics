#include "task4.h"

void fractional(double &a)
{
    a = static_cast<int> (a);
}

void fractional(float &a)
{
    a = (int) (a);
}

void fractional(double *a)
{
    *a = static_cast<int> (*a);
}

void fractional(float *a)
{
    *a = static_cast<int> (*a);
}

