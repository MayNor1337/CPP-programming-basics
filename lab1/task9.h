#ifndef LAB1_TASK9_H
#define LAB1_TASK9_H

struct Complex {
    double real;
    double imag;
};

void ref_multiplication(struct Complex &a, double &b);
void pointer_multiplication(struct Complex *a, double *b);

#endif //LAB1_TASK9_H
