cmake_minimum_required(VERSION 3.23)
project(lab1)

set(CMAKE_CXX_STANDARD 17)

add_executable(lab1 main.cpp task1.cpp task1.h task4.cpp task4.h task9.cpp task9.h task12.cpp task12.h)
