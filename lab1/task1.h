#ifndef LAB1_TASK1_H
#define LAB1_TASK1_H

template<typename T>
void ref_swap(T &a, T &b)
{
    T c = b;
    b = a;
    a = c;
}

template<typename T>
void pointer_swap(T *a, T *b)
{
    T c = *b;
    *b = *a;
    *a = c;
}

#endif //LAB1_TASK1_H
