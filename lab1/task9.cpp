#include "task9.h"


void ref_multiplication(struct Complex &a, double &b)
{
    a.real = a.real * b;
    a.imag = a.imag * b;
}

void pointer_multiplication(struct Complex *a, double *b)
{
    a->real = a->real * *b;
    a->imag = a->imag * *b;
}