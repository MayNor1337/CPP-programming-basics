#include <iostream>
#include "task1.h"
#include "task4.h"

int main() {

    ///// task 1
//    std::cout << "Enter 2 numbers" << std::endl;
//    int a, b;
//    std::cin >> a >> b;
//
//    ref_swap<int>(a, b);
//    std::cout << a << b;
//    pointer_swap(&a, &b);
//    std::cout << a << b;

    //// task 4
    std::cout << "Enter a fractional number\n";
    double _fractional;
    std::cin >> _fractional;
    double c = _fractional;
    fractional(c);
    std::cout << c << '\n';
    float d = _fractional;
    fractional((double)d);
    std::cout << d << '\n';

    c = _fractional;
    fractional(&c);
    std::cout << c << '\n';
    d = _fractional;
    fractional(&d);
    std::cout << d << '\n';

    return 0;
}
