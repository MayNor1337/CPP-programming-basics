#ifndef LAB1_TASK12_H
#define LAB1_TASK12_H

struct circle
{
    double x, y;
    double r;
}

void ref_rotate(struct circle &s, double &delta_x, double &delta_y);
void pointer_rotate(struct circle *s, double *delta_x, double *delta_y);

#endif //LAB1_TASK12_H
